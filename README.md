Estimator
=========

[![Pipeline Status](https://gitlab.com/chrisspen/estimator/badges/master/pipeline.svg)](https://gitlab.com/chrisspen/estimator/commits/master) 
[![PyPI Status](https://img.shields.io/pypi/v/task-estimator.svg)](https://pypi.python.org/pypi/task-estimator)

A Python script for automatically estimating work hours in Jira using supervised learning.

Installation
------------

Install the Python 3 package:

    pip install estimator

Usage
-----

Fill out the login credentials in your `config.yaml`. A sample file to pull data from Jira would look like:

    source: jira
    server: https://myname.atlassian.net
    username: myusername
    password: mypassword
    projects: MYPROJECTKEY
    regressor:
        cls: KernelRidge
        stop_words: ''
        ngram_range: [1, 6]
        analyzer: char
        min_df: 0.0
    minimum_estimate_minutes: 15
    hour_update_fields:
    -   Story Points

Retrieve training data:

    estimator config.yaml retrieve

Generate all possible algorithm and setting combinations:

    estimator config.yaml generation-combinations

Test all combinations to find which algorithm works best:

    estimator config.yaml test-combinations

Fill in the `regressor` settings in your `config.yaml`, then train your final regressor:

    estimator config.yaml train

Generate a report of estimation accuracy:

    estimator config.yaml test

Dryrun the application of the regressor to unestimated tickets:

    estimator config.yaml apply

Save these estimates with:

    estimator config.yaml apply --save

To combine the above retrieve, train and apply steps into a single command, just add `--retrain` to the apply command:

    estimator config.yaml apply --save --retrain

Development
-----------

Run tests locally with:

    tox

To run tests for a specific environment (e.g. Python 3.7):
    
    tox -e py37

To run a specific test:
    
    export TESTNAME=.test_learning; tox -e py37
