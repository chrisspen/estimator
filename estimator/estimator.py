#!/usr/bin/env python
import sys
import os
import argparse
import csv
import re
import pickle
import random
import time
import logging
from datetime import date, timedelta
from pprint import pprint

import yaml

from jira import JIRA

import pandas as pd
# import numpy as np
# from scipy.spatial import distance
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import mean_absolute_error, median_absolute_error
# from sklearn.pipeline import Pipeline
# from sklearn.preprocessing import StandardScaler, MinMaxScaler, MaxAbsScaler
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.linear_model import LogisticRegression # note, not included in all_estimators?
# from sklearn.linear_model.bayes import ARDRegression
from sklearn.ensemble.weight_boosting import AdaBoostRegressor
from sklearn.ensemble.bagging import BaggingRegressor
# from sklearn.linear_model.bayes import BayesianRidge
# from sklearn.cross_decomposition.cca_ import CCA
from sklearn.tree.tree import DecisionTreeRegressor
# from sklearn.linear_model.coordinate_descent import ElasticNet
# from sklearn.linear_model.coordinate_descent import ElasticNetCV
from sklearn.tree.tree import ExtraTreeRegressor
from sklearn.ensemble.forest import ExtraTreesRegressor
# from sklearn.gaussian_process.gpr import GaussianProcessRegressor
from sklearn.ensemble.gradient_boosting import GradientBoostingRegressor
from sklearn.linear_model.huber import HuberRegressor
# from sklearn.neighbors.regression import KNeighborsRegressor
from sklearn.kernel_ridge import KernelRidge
# from sklearn.linear_model.least_angle import Lars
# from sklearn.linear_model.least_angle import LarsCV
# from sklearn.linear_model.coordinate_descent import Lasso
# from sklearn.linear_model.coordinate_descent import LassoCV
# from sklearn.linear_model.least_angle import LassoLars
# from sklearn.linear_model.least_angle import LassoLarsCV
# from sklearn.linear_model.least_angle import LassoLarsIC
from sklearn.linear_model.base import LinearRegression
from sklearn.svm.classes import LinearSVR
from sklearn.neural_network.multilayer_perceptron import MLPRegressor
# from sklearn.linear_model.coordinate_descent import MultiTaskElasticNet
# from sklearn.linear_model.coordinate_descent import MultiTaskElasticNetCV
# from sklearn.linear_model.coordinate_descent import MultiTaskLasso
# from sklearn.linear_model.coordinate_descent import MultiTaskLassoCV
from sklearn.svm.classes import NuSVR
# from sklearn.linear_model.omp import OrthogonalMatchingPursuit
# from sklearn.linear_model.omp import OrthogonalMatchingPursuitCV
# from sklearn.cross_decomposition.pls_ import PLSCanonical
# from sklearn.cross_decomposition.pls_ import PLSRegression
from sklearn.linear_model.passive_aggressive import PassiveAggressiveRegressor
from sklearn.linear_model.ransac import RANSACRegressor
# from sklearn.neighbors.regression import RadiusNeighborsRegressor
from sklearn.ensemble.forest import RandomForestRegressor
from sklearn.linear_model.ridge import Ridge
from sklearn.linear_model.ridge import RidgeCV
from sklearn.linear_model.stochastic_gradient import SGDRegressor
from sklearn.svm.classes import SVR
# from sklearn.linear_model.theil_sen import TheilSenRegressor
from sklearn.compose._target import TransformedTargetRegressor

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s", "%Y-%m-%d %H:%M:%S")
handler.setFormatter(formatter)
logger.addHandler(handler)

CACHE_DIR = 'cache'

SOURCE_JIRA = 'jira'
SOURCE_FIXTURE = 'fixture'
SOURCES = (
    SOURCE_JIRA,
    SOURCE_FIXTURE,
)

JIRA_DONE_STATUSES = 'Closed, Deployed, "Ready For Deployment", Resolved'
JIRA_OPEN_STATUSES = 'Open, "Ready For Development"'

ACTION_RETRIEVE = 'retrieve'
ACTION_TRAIN = 'train'
ACTION_TEST = 'test'
ACTION_APPLY = 'apply'
ACTION_GENERATE_COMBINATIONS = 'generate-combinations'
ACTION_TEST_COMBINATIONS = 'test-combinations'
ACTION_LIST_REGRESSORS = 'list-regressors'
ACTION_TEST_LOGIN = 'test-login'
ACTION_CLEAN = 'clean'
ACTIONS = (
    ACTION_RETRIEVE,
    ACTION_TRAIN,
    ACTION_TEST,
    ACTION_APPLY,
    ACTION_GENERATE_COMBINATIONS,
    ACTION_TEST_COMBINATIONS,
    ACTION_LIST_REGRESSORS,
    ACTION_TEST_LOGIN,
    ACTION_CLEAN,
)

stemmer = SnowballStemmer('english')

USER_ESTIMATION_CACHE_FN = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../cache/user_estimations.csv')

# Custom class that adapts sklearn's CountVectorizer to include a stemmer
# http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html
class StemmedCountVectorizer(CountVectorizer):
    def build_analyzer(self):
        analyzer = super(StemmedCountVectorizer, self).build_analyzer()
        return lambda doc: ([stemmer.stem(w) for w in analyzer(doc)])


def iter_search_issues(j, jql):
    block_size = 100
    block_num = 0
    while True:
        start_idx = block_num * block_size
        issues = j.search_issues(jql, start_idx, block_size)
        if not issues:
            # Retrieve issues until there are no more to come
            break
        block_num += 1
        for issue in issues:
            yield issue


ISSUE_FIELD = 'issue'
TITLE_FIELD = 'title'
TEXT_FIELD = 'text'
HOURS_FIELD = 'hours'
ESTIMATED_HOURS_FIELD = 'estimated_hours'
ESTIMATING_USER_FIELD = 'estimating_user'
STORY_POINTS = 'Story Points'
TIME_ORIGINAL_ESTIMATE = 'timeoriginalestimate'

DATA_TRAINING = 'training'
DATA_TESTING = 'testing'
DATA_HUMAN = 'human'

USER_ESTIMATIONS = {}
USER_ESTIMATION_FIELDS = ['ticket', 'user', 'original_estimate', 'estimation_date']
USER_ESTIMATIONS_UNSAVED = 0

HUMAN = 'human'
ALGO = 'algo'

def mean_error(a, b):
    assert len(a) == len(b)
    return sum(_a - _b for _a, _b in zip(a, b))/float(len(a))

def init_user_estimations():
    if os.path.exists(USER_ESTIMATION_CACHE_FN):
        return
    fieldnames = USER_ESTIMATION_FIELDS
    with open(USER_ESTIMATION_CACHE_FN, 'w') as fout:
        writer = csv.DictWriter(fout, fieldnames=fieldnames)
        writer.writerow(dict(zip(fieldnames, fieldnames)))

def load_user_estimations():
    init_user_estimations()
    reader = csv.DictReader(open(USER_ESTIMATION_CACHE_FN))
    for row in reader:
        USER_ESTIMATIONS[row['ticket']] = (row['user'], float(row['original_estimate']), row['estimation_date'])

def save_user_estimations():
    global USER_ESTIMATIONS_UNSAVED # pylint: disable=global-statement
    fieldnames = USER_ESTIMATION_FIELDS
    with open(USER_ESTIMATION_CACHE_FN, 'w') as fout:
        writer = csv.DictWriter(fout, fieldnames=fieldnames)
        writer.writerow(dict(zip(fieldnames, fieldnames)))
        for ticket in sorted(USER_ESTIMATIONS):
            user, original_estimate, estimation_date = USER_ESTIMATIONS[ticket]
            writer.writerow({'ticket': ticket, 'user': user, 'original_estimate': original_estimate, 'estimation_date': estimation_date})
    USER_ESTIMATIONS_UNSAVED = 0

def get_user_estimate(ticket):
    return USER_ESTIMATIONS.get(ticket)

def add_user_estimation(ticket, user, original_estimate, estimation_date):
    global USER_ESTIMATIONS_UNSAVED # pylint: disable=global-statement
    USER_ESTIMATIONS_UNSAVED += 1
    USER_ESTIMATIONS[ticket] = (user, original_estimate, estimation_date)
    if USER_ESTIMATIONS_UNSAVED >= 100:
        save_user_estimations()

load_user_estimations()

class Estimator:

    def __init__(self, name, source, verbose=False, **kwargs):
        self.verbose = verbose
        self.name = name
        self.source = source
        self.data_dir = kwargs.pop('data_dir', 'data')
        self.fixture = kwargs.pop('fixture', '')
        self.regressor_params = kwargs.pop('regressor', {})
        self.extra_sql = kwargs.pop('extra_sql', '').strip()
        self.minimum_estimate_minutes = int(kwargs.pop('minimum_estimate_minutes', '30'))
        # These fields will be updated with the hourly estimate.
        self.hour_update_fields = kwargs.pop('hour_update_fields', [])
        self.bot_accounts = kwargs.pop('bot_accounts', [])
        self.ignore_accounts = kwargs.pop('ignore_accounts', [])
        self._training_fn = None
        self._label_to_field = None
        if source == SOURCE_JIRA:
            self.server = kwargs.pop('server')
            self.username = kwargs.pop('username')
            self.password = kwargs.pop('password')
            self.projects = kwargs.pop('projects')
            options = {'server': self.server}
            self._log_vinfo('Logging into Jira at server %s with username %s and password %s.', self.server, self.username, self.password)
            self.jira = JIRA(options=options, basic_auth=(self.username, self.password))
        elif source == SOURCE_FIXTURE:
            self.projects = kwargs.pop('projects')
        else:
            raise NotImplementedError('Unknown source: %s' % source)

    def _log_vinfo(self, *args, **kwargs):
        if self.verbose:
            logger.info(*args, **kwargs)

    @classmethod
    def from_configuration_file(cls, fn, verbose=False):
        with open(fn, 'r') as fin:
            config = yaml.load(fin)
        return cls(os.path.splitext(os.path.split(fn)[-1])[0], verbose=verbose, **config)

    @property
    def training_fn(self):
        """
        Filename to use for storing tagged sample records, suitable for training.
        """
        return self._training_fn or ('%s/%s-training.csv' % (self.data_dir, self.name))

    @property
    def untagged_fn(self):
        """
        Filename to use for storing untagged sample records, sutiable for application of a trained regressor.
        """
        return self._training_fn or ('%s/%s-untagged.csv' % (self.data_dir, self.name))

    @property
    def classifier_fn(self):
        return '%s/%s-classifier.pkl' % (self.data_dir, self.name)

    @property
    def vectorizer_fn(self):
        return '%s/%s-vectorizer.pkl' % (self.data_dir, self.name)

    @property
    def tfidf_fn(self):
        return '%s/%s-tfidf.pkl' % (self.data_dir, self.name)

    @property
    def label_to_field(self):
        """
        Returns a dictionary mapping the user friendly name into the system's internal name used for doing database updates.
        """
        if not self._label_to_field:
            if self.source == SOURCE_JIRA:
                self._label_to_field = {field['name']: field['id'] for field in self.jira.fields()}
            else:
                raise NotImplementedError
        return self._label_to_field

    def retrieve(self, training=True, key=None, has_label=False):
        self.verbose = 1

        # Set filename for saving data.
        if training:
            fn = self.training_fn
        else:
            fn = self.untagged_fn

        if self.source == SOURCE_JIRA:
            if key:
                jql = 'key = {key}'.format(key=key)
            else:
                jql = 'project = "{projects}"'
                if training:
                    # If we're looking for training data, then ensure all issues have logged time.
                    jql += ' AND timeSpent IS NOT EMPTY AND status IN ({done_statuses})'
                    if has_label:
                        # Only look at values where someone has left logged hours.
                        jql += ' AND originalEstimate IS NOT EMPTY'
                        # jql += ' AND (originalEstimate IS EMPTY OR "Story Points" IS EMPTY)'
                else:
                    # If we're NOT looking for training data, then ensure all issues have no original time estimate.
                    jql += ' AND originalEstimate IS EMPTY AND status IN ({open_statuses})'
                jql = jql.format(projects=self.projects, done_statuses=JIRA_DONE_STATUSES, open_statuses=JIRA_OPEN_STATUSES)
                if self.extra_sql:
                    jql += ' AND %s' % self.extra_sql

            # Only look at last N days.
            jql += ' AND created >= %s' % (date.today() - timedelta(days=365))

            allfields = self.jira.fields()
            label_to_field = {field['name']: field['id'] for field in allfields}
            if self.verbose:
                logger.info('Jira fields:')
                pprint(label_to_field, indent=4)

            if self.verbose:
                logger.info('Jira query: %s', jql)

            fieldnames = [ISSUE_FIELD, TITLE_FIELD, TEXT_FIELD, HOURS_FIELD, ESTIMATED_HOURS_FIELD, ESTIMATING_USER_FIELD]
            writer = csv.DictWriter(open(fn, 'w'), fieldnames=fieldnames)
            writer.writerow(dict(zip(fieldnames, fieldnames)))
            for i, issue in enumerate(iter_search_issues(self.jira, jql)):
                logger.info('Retrieving data for issue %s.', issue)
                issue = self.jira.issue(str(issue), expand='changelog')

                if training:
                    hours = issue.fields.timespent * 1/60. * 1/60.
                else:
                    hours = None

                estimated_hours = None
                if issue.fields.timeoriginalestimate:
                    estimated_hours = issue.fields.timeoriginalestimate * 1/60. * 1/60.

                # Lookup estimating user.
                estimating_user = None
                estimation_date = None
                _estimate = get_user_estimate(str(issue))
                logger.info('Estimate: %s %s', str(issue), _estimate)
                if _estimate is None:
                    for history in issue.changelog.histories:
                        author = str(history.author)
                        for item in history.items:
                            if item.field == TIME_ORIGINAL_ESTIMATE:
                                _original_estimate_seconds = int(item.toString or 0) * 1/60. * 1/60.
                                logger.info('User %s estimated %s hours.', author, _original_estimate_seconds)
                                if _original_estimate_seconds == estimated_hours:
                                    estimation_date = history.created
                                    estimating_user = author
                                    break
                                else:
                                    logger.info('Ignoring because %s != %s.', _original_estimate_seconds, estimated_hours)

                    # Check for conflicting story point estimate.
                    story_points = 0
                    story_point_user = None
                    for history in issue.changelog.histories:
                        for item in history.items:
                            if item.field == STORY_POINTS:
                                logger.info('Found story points %s for issue %s.', item.toString, issue)
                                story_points = float(item.toString or 0)
                                story_point_user = str(history.author)

                    # If the story points matches the original estimate, then the estimator is the story point user.
                    if story_point_user and estimated_hours == story_points:
                        logger.info('Found story points override for issue %s by user %s!', issue, story_point_user)
                        estimating_user = story_point_user

                    # Cache estimation data so we don't have to parse issue history each time.
                    if estimating_user is not None:
                        add_user_estimation(ticket=str(issue), user=estimating_user, original_estimate=estimated_hours, estimation_date=estimation_date)

                else:
                    estimating_user, _, _ = _estimate

                # We don't know who made the estimate, so skip.
                if not estimating_user:
                    continue

                text = re.sub(r'[\n\t\r, ]+', ' ', issue.fields.description or '').strip()
                title = (issue.fields.summary or '').strip()
                data = {
                    ISSUE_FIELD: str(issue),
                    TITLE_FIELD: title,
                    TEXT_FIELD: text,
                    HOURS_FIELD: hours,
                    ESTIMATED_HOURS_FIELD: estimated_hours,
                    ESTIMATING_USER_FIELD: estimating_user,
                }
                writer.writerow(data)
            save_user_estimations()
        elif self.source == SOURCE_FIXTURE:
            self._training_fn = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'fixtures/%s.csv' % self.fixture)
            assert os.path.isfile(self._training_fn), 'File %s does not exist.' % self._training_fn
        else:
            raise NotImplementedError

    def get_training_dataframe(self, typ=None):
        assert typ is None or typ in (HUMAN, ALGO), 'Invalid typ: %s' % typ
        reader = list(csv.DictReader(open(self.training_fn)))
        data = []
        for row in reader:
            if row[ESTIMATING_USER_FIELD] in self.ignore_accounts:
                continue
            if typ:
                if typ == HUMAN and row[ESTIMATING_USER_FIELD] in self.bot_accounts:
                    continue
                elif typ == ALGO and row[ESTIMATING_USER_FIELD] not in self.bot_accounts:
                    continue
            try:
                row[HOURS_FIELD] = float(row[HOURS_FIELD]) # actual true logged data
            except ValueError:
                logger.warning('Warning: Ignoring bad hours value: %s', row[HOURS_FIELD])
                continue
            try:
                row[ESTIMATED_HOURS_FIELD] = float(row[ESTIMATED_HOURS_FIELD]) # expected data
            except ValueError:
                logger.warning('Warning: Ignoring bad estimated_hours value: %s', row[ESTIMATED_HOURS_FIELD])
                continue

            if row[HOURS_FIELD] == row[ESTIMATED_HOURS_FIELD]:
                logger.warning('Warning: Ignoring perfect estimates for issue %s.', row[ISSUE_FIELD])
                continue

            data.append(row)
        df = pd.DataFrame(data)
        return df

    def get_untagged_dataframe(self):
        reader = list(csv.DictReader(open(self.untagged_fn)))
        df = pd.DataFrame(reader)
        return df

    def list_regressors(self):
        """
        Lists all regressors available.
        """
        from sklearn.utils.testing import all_estimators
        lst = all_estimators(type_filter='regressor')
        regressors = []
        for name, cls in sorted(lst, key=lambda o: o[0]):
            regressors.append(name)
            path = re.findall(r'\'([^\']+)\'', str(cls))[0].split('.')
            from_part = '.'.join(path[:-1])
            module_part = path[-1]
            print('from %s import %s' % (from_part, module_part))
        print()
        for r in sorted(regressors):
            print('%s,' % r)

    def generate_combinations(self):
        """
        Generates all parameter combinations for training regressors.
        """
        from itertools import product
        cls = [
            LogisticRegression, # not included by all_estimators by default?

            # ARDRegression, # does not support sparse data
            AdaBoostRegressor,
            BaggingRegressor,
            # BayesianRidge, # does not support sparse data
            # CCA, # does not support sparse data
            DecisionTreeRegressor,
            # ElasticNet, # works, but takes forever to train and accuracy is poor
            # ElasticNetCV, # works, but takes forever to train and accuracy is poor
            ExtraTreeRegressor,
            ExtraTreesRegressor,
            # GaussianProcessRegressor, # does not support sparse data
            GradientBoostingRegressor,
            HuberRegressor,
            # KNeighborsRegressor, # does not support sparse data
            KernelRidge,
            # Lars, # does not support sparse data
            # LarsCV, # does not support sparse data
            # Lasso, # redundant with LassoCV
            # LassoCV, # takes ~10 hours to train but has mediocre accuracy
            # LassoLars, # does not support sparse data
            # LassoLarsCV, # does not support sparse data
            # LassoLarsIC, # does not support sparse data
            LinearRegression,
            LinearSVR,
            MLPRegressor,
            # MultiTaskElasticNet, # does not support sparse data
            # MultiTaskElasticNetCV, # does not support sparse data
            # MultiTaskLasso, # does not support sparse data
            # MultiTaskLassoCV, # does not support sparse data
            NuSVR,
            # OrthogonalMatchingPursuit, # does not support sparse data
            # OrthogonalMatchingPursuitCV, # does not support sparse data
            # PLSCanonical, # does not support sparse data
            # PLSRegression, # does not support sparse data
            PassiveAggressiveRegressor,
            RANSACRegressor,
            # RadiusNeighborsRegressor, # does not support sparse data
            RandomForestRegressor,
            Ridge,
            RidgeCV,
            SGDRegressor,
            SVR,
            # TheilSenRegressor, # does not support sparse data
            TransformedTargetRegressor,
        ]
        stop_words = ['english', None]
        min_df = [
            0,
            0.25,
            0.5,
            # 0.75,
            # 1.0
        ]
        analyzer = ['word', 'char', 'char_wb']
        ngram_range = [
            # (1, 1),
            (1, 2),
            (1, 3),
            (1, 4),
            (1, 5),
            (1, 6),
            (2, 2),
            # (2, 3),
            # (2, 4),
            # (2, 5),
            # (2, 6),
            # (3, 3),
            # (3, 4),
            # (3, 5),
            # (3, 6),
        ]
        fieldnames = ['cls', 'stop_words', 'min_df', 'analyzer', 'ngram_min', 'ngram_max']
        writer = csv.DictWriter(open(self.combinations_fn, 'w'), fieldnames=fieldnames)
        writer.writerow(dict(zip(fieldnames, fieldnames)))
        all_combos = list(product(cls, stop_words, min_df, analyzer, ngram_range))
        random.shuffle(all_combos)
        for _cls, _stop_words, _min_df, _analyzer, _ngram_range in all_combos:
            writer.writerow({
                'cls': _cls.__name__,
                'stop_words': _stop_words,
                'min_df': _min_df,
                'analyzer': _analyzer,
                'ngram_min': _ngram_range[0],
                'ngram_max': _ngram_range[1]
            })

    @property
    def combinations_fn(self):
        return '%s/%s-combinations.csv' % (self.data_dir, self.name)

    @property
    def priors_fn(self):
        return '%s/%s-combinations-priors.txt' % (self.data_dir, self.name)

    @property
    def scores_fn(self):
        return '%s/%s-combinations-scores.csv' % (self.data_dir, self.name)

    def test_combinations(self):
        """
        Iterates through all combinations, trains a regressor and records its error rate.
        """
        reader = csv.DictReader(open(self.combinations_fn, 'r'))
        try:
            priors = set(l.strip() for l in open(self.priors_fn, 'r').readlines())
        except FileNotFoundError:
            priors = set()
        logger.info('%i priors found.', len(priors))
        write_headers = not os.path.isfile(self.scores_fn)
        with open(self.scores_fn, 'a') as fout, open(self.priors_fn, 'a') as prior_out:
            fieldnames = ['cls', 'stop_words', 'min_df', 'analyzer', 'ngram_min', 'ngram_max', 'mse', 'time']
            writer = csv.DictWriter(fout, fieldnames=fieldnames)

            # Write header.
            if write_headers:
                writer.writerow(dict(zip(fieldnames, fieldnames)))

            # Check every combination.
            combos = list(reader)
            total = len(combos)
            i = 0
            for combo in combos:
                i += 1
                sys.stdout.write('Checking combo %i of %i: %.02f%% %s...\n' % (i, total, i/float(total)*100, combo))
                sys.stdout.flush()

                # Skip combinations that we've already tested.
                combo_hash = str(tuple(sorted(combo.items())))
                if combo_hash in priors:
                    continue

                # Convert combo values.
                cls = eval(combo['cls']) # pylint: disable=eval-used
                stop_words = combo['stop_words']
                if not stop_words or stop_words == 'None':
                    stop_words = None
                ngram_range = (int(combo['ngram_min']), int(combo['ngram_max']))
                min_df = float(combo['min_df'])
                analyzer = combo['analyzer']

                # Find combo MSE.
                td = None
                try:
                    t0 = time.time()
                    clf, count_vect, tfidf_transformer = self.train(
                        cls=cls,
                        stop_words=stop_words,
                        ngram_range=ngram_range,
                        analyzer=analyzer,
                        min_df=min_df,
                        save=False
                    )
                    td = time.time() - t0
                    error = self.test(clf=clf, count_vect=count_vect, tfidf_transformer=tfidf_transformer)
                except Exception as exc:
                    error = str(exc)
                    print('error:', error)

                # Save results.
                writer.writerow({
                    'cls': cls.__name__,
                    'stop_words': stop_words,
                    'min_df': min_df,
                    'analyzer': analyzer,
                    'ngram_min': ngram_range[0],
                    'ngram_max': ngram_range[1],
                    'mse': error,
                    'time': td,
                })
                fout.flush()
                print('TD:', td)
                print(combo_hash, file=prior_out)
                prior_out.flush()

    def train(self, cls=None, stop_words=-1, ngram_range=None, analyzer=None, min_df=None, save=True):
        """
        Trains a regressor using the given parameters.
        """

        if cls is None:
            cls = self.regressor_params.get('cls', LinearRegression)
            if isinstance(cls, str):
                cls = globals()[cls]
        if stop_words == -1:
            stop_words = self.regressor_params.get('stop_words', 'english')
            if not stop_words:
                stop_words = None
        if ngram_range is None:
            ngram_range = tuple(self.regressor_params.get('ngram_range', (1, 3)))
        if analyzer is None:
            analyzer = self.regressor_params.get('analyzer', 'word')
        if min_df is None:
            min_df = float(self.regressor_params.get('min_df', 0.0))

        logger.info('Training using: %s %s %s %s %s', cls, stop_words, ngram_range, analyzer, min_df)

        logger.info('Loading training dataframe...')
        t0 = time.time()
        df = self.get_training_dataframe()
        td = time.time() - t0
        logger.info('Training dataframe loaded in %s seconds.', td)

        # https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html#sklearn.feature_extraction.text.CountVectorizer
        logger.info('Training vectorizer...')
        t0 = time.time()
        count_vect = StemmedCountVectorizer(stop_words=stop_words, ngram_range=ngram_range, analyzer=analyzer, min_df=min_df)
        # X_train_counts = count_vect.fit_transform(df[TEXT_FIELD])
        X_train_counts = count_vect.fit_transform(df[TITLE_FIELD] + ' ' + df[TEXT_FIELD])
        td = time.time() - t0
        logger.info('Trained vectorizer in %s seconds.', td)

        logger.info('Training tfidf...')
        t0 = time.time()
        tfidf_transformer = TfidfTransformer()
        X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
        td = time.time() - t0
        logger.info('Trained tfidf in %s seconds.', td)

        logger.info('Training classifier...')
        t0 = time.time()
        clf = cls()
        clf.fit(X_train_tfidf, df[HOURS_FIELD])
        td = time.time() - t0
        logger.info('Trained classifier in %s seconds.', td)

        def make_dirs(fn):
            try:
                d, f = os.path.split(fn)
                if d:
                    os.makedirs(d)
            except Exception as exc:
                logger.error('Unable to make directory %s: %s', d, exc)

        if save:

            logger.info('Saving classifier...')
            make_dirs(self.classifier_fn)
            with open(self.classifier_fn, 'wb') as fout:
                pickle.dump(clf, fout)

            # Dump the trained vectorizer with Pickle
            logger.info('Saving vectorizer...')
            make_dirs(self.vectorizer_fn)
            with open(self.vectorizer_fn, 'wb') as fout:
                pickle.dump(count_vect, fout)

            # Dump the trained tfidf with Pickle
            logger.info('Saving tfidf...')
            make_dirs(self.tfidf_fn)
            with open(self.tfidf_fn, 'wb') as fout:
                pickle.dump(tfidf_transformer, fout)

        return clf, count_vect, tfidf_transformer

    def test(self, clf=None, count_vect=None, tfidf_transformer=None, human=False, retrain=False, export=True, no_cache=False):
        """
        Calculates the mean squared error of the last trained regressor.
        """

        data = {} # {ticket: [actual hours, human estimated hours, algo estimated hours]}
        y_true_human = []
        y_pred_human = []
        y_true_algo = []
        y_pred_algo = []
        mae_per_human_true = {}
        mae_per_human_pred = {}
        mae_human = None
        mae_algo = None
        median_ae_human = None
        median_ae_algo = None
        me_human = None
        me_algo = None

        user_perfect_count = {} # {user: number of times they had a perfect estimate, indicating cheating}
        user_total_count = {} # {user: total number of estimations}

        os.makedirs(CACHE_DIR, exist_ok=True)

        ## Calculate human accuracy.

        logger.info('Calculating the error rate in human provided estimates...')
        _df_human_fn = f'{CACHE_DIR}/_df_human_{date.today()}.pkl'
        if not no_cache and os.path.isfile(_df_human_fn):
            logger.info('Loading pickle for human dataframe from %s...', _df_human_fn)
            df = pd.read_pickle(_df_human_fn)
        else:
            self.retrieve(training=True, has_label=True)
            df = self.get_training_dataframe(typ=HUMAN)
            logger.info('Saving pickle for human dataframe to %s...', _df_human_fn)
            df.to_pickle(_df_human_fn)
        if df.columns.any():
            all_rows = list(df.iterrows())
            total = len(all_rows)
            i = 0
            for index, row in all_rows:
                i += 1
                logger.info('Processing human record %i of %i.', i, total)
                if bool(row['hours']) and bool(row['estimated_hours']):
                    username = row['estimating_user']
                    actual_hours = float(row['hours'])
                    estimated_hours = float(row['estimated_hours'])
                    issue = row['issue']
                    if actual_hours == estimated_hours:
                        logger.warning('Ignoring issue %s due to identical estimated and logged hours.', issue)
                        continue
                    y_true_human.append(actual_hours)
                    y_pred_human.append(estimated_hours)
                    data.setdefault(issue, [None, None, None])
                    data[issue][0] = actual_hours
                    data[issue][1] = estimated_hours

                    mae_per_human_true.setdefault(username, [])
                    mae_per_human_true[username].append(actual_hours)

                    mae_per_human_pred.setdefault(username, [])
                    mae_per_human_pred[username].append(estimated_hours)

                    user_perfect_count.setdefault(username, 0)
                    user_perfect_count[username] += actual_hours == estimated_hours
                    user_total_count.setdefault(username, 0)
                    user_total_count[username] += 1
            logger.info("Test Samples: %s", len(y_pred_human))
            mae_human = mean_absolute_error(y_true_human, y_pred_human)
            median_ae_human = median_absolute_error(y_true_human, y_pred_human)
            me_human = mean_error(y_true_human, y_pred_human)
        else:
            logger.warning('No data found for human estimates.')

        ## Calculate algorithm accuracy.

        logger.info('Loading algo data...')
        _df_algo_fn = f'{CACHE_DIR}/_df_algo_{date.today()}.pkl'
        if not no_cache and os.path.isfile(_df_algo_fn):
            logger.info('Loading pickle for algo dataframe from %s...', _df_algo_fn)
            df = pd.read_pickle(_df_algo_fn)
        else:
            df = self.get_training_dataframe(typ=ALGO)
            logger.info('Saving pickle for algo dataframe to %s...', _df_algo_fn)
            df.to_pickle(_df_algo_fn)

        predictions = []
        if df.columns.any():
            for col in df.columns:
                logger.info('Column found: %s', col)

            if clf is None:
                logger.info('Loading regressor from %s.', self.classifier_fn)
                clf = pickle.load(open(self.classifier_fn, 'rb'))

            if count_vect is None:
                logger.info('Loading vectorizer from %s.', self.vectorizer_fn)
                count_vect = pickle.load(open(self.vectorizer_fn, 'rb'))

            if tfidf_transformer is None:
                logger.info('Loading transformer %s.', self.tfidf_fn)
                tfidf_transformer = pickle.load(open(self.tfidf_fn, 'rb'))

            logger.info('Vectorizing data...')
            t0 = time.time()
            X_train_counts = count_vect.fit_transform(df[TITLE_FIELD] + ' ' + df[TEXT_FIELD])
            td = time.time() - t0
            logger.info('Vectorized in %s seconds.', td)

            logger.info('Transforming data...')
            t0 = time.time()
            X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
            td = time.time() - t0
            logger.info('Transformed in %s seconds.', td)

            logger.info('Running cross validation...')
            t0 = time.time()
            # https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.cross_val_predict.html
            predictions = cross_val_predict(clf, X_train_tfidf, df[HOURS_FIELD])
            td = time.time() - t0
            logger.info('Ran cross validation in %s seconds.', td)

            all_rows = list(df.iterrows())
            total = len(all_rows)
            i = 0
            for index, row in all_rows:
                i += 1
                logger.info('Processing algo record %i of %i.', i, total)
                if bool(row['hours']) and bool(row['estimated_hours']):
                    username = row['estimating_user']
                    issue = row['issue']
                    actual_hours = float(row['hours'])
                    estimated_hours = float(row['estimated_hours'])
                    y_true_algo.append(actual_hours)
                    y_pred_algo.append(estimated_hours)
                    data.setdefault(issue, [None, None, None])
                    data[issue][0] = actual_hours
                    data[issue][2] = estimated_hours

                    user_perfect_count.setdefault(username, 0)
                    user_perfect_count[username] += actual_hours == estimated_hours
                    user_total_count.setdefault(username, 0)
                    user_total_count[username] += 1

            logger.info("Test Samples: %s", len(predictions))
            mae_algo = mean_absolute_error(y_true_algo, y_pred_algo)
            median_ae_algo = median_absolute_error(y_true_algo, y_pred_algo)
            me_algo = mean_error(y_true_algo, y_pred_algo)

        else:
            logger.warning('No data found for algorithm estimates.')

        if export:
            report_fn = 'reports/estimator-issue-stats-human.csv'
            with open(report_fn, 'w') as fout:
                fieldnames = ['issue', 'estimated hours', 'human logged hours']
                writer = csv.DictWriter(fout, fieldnames=fieldnames)
                writer.writerow(dict(zip(fieldnames, fieldnames)))
                for issue in sorted(data):
                    human_logged_hours, human_estimated_hours, algo_estimated_hours = data[issue]
                    if not human_estimated_hours:
                        continue
                    writer.writerow({
                        'issue': issue,
                        'estimated hours': human_estimated_hours,
                        'human logged hours': human_logged_hours,
                    })
                writer.writerow({
                    'issue': 'Mean Absolute Error',
                    'estimated hours': mae_human,
                })
            logger.info('Wrote human issue report to %s.', report_fn)

            report_fn = 'reports/estimator-issue-stats-algo.csv'
            with open(report_fn, 'w') as fout:
                fieldnames = ['issue', 'estimated hours', 'human logged hours']
                writer = csv.DictWriter(fout, fieldnames=fieldnames)
                writer.writerow(dict(zip(fieldnames, fieldnames)))
                for issue in sorted(data):
                    human_logged_hours, human_estimated_hours, algo_estimated_hours = data[issue]
                    if not algo_estimated_hours:
                        continue
                    writer.writerow({
                        'issue': issue,
                        'estimated hours': algo_estimated_hours,
                        'human logged hours': human_logged_hours,
                    })
                writer.writerow({
                    'issue': 'Mean Absolute Error',
                    'estimated hours': mae_algo,
                })
            logger.info('Wrote algo issue report to %s.', report_fn)

            user_report_fn = 'reports/estimator-user-stats.csv'
            with open(user_report_fn, 'w') as fout:
                fieldnames = ['username', 'total estimates', 'perfect estimate count', 'perfect ratio']
                writer = csv.DictWriter(fout, fieldnames=fieldnames)
                writer.writerow(dict(zip(fieldnames, fieldnames)))
                for username in sorted(user_perfect_count):
                    writer.writerow({
                        'username': username,
                        'total estimates': user_total_count[username],
                        'perfect estimate count': user_perfect_count[username],
                        'perfect ratio': round(user_perfect_count[username]/user_total_count[username], 2),
                    })
            logger.info('Wrote user report to %s.', user_report_fn)

        logger.info('Algo Stats:')
        logger.info("Mean Absolute Error (algo): %s", mae_algo)
        logger.info("Mean Error (algo): %s", me_algo)
        logger.info("Median Absolute Error (algo): %s", median_ae_algo)

        logger.info('Human Stats:')
        logger.info("Mean Absolute Error (human): %s", mae_human)
        logger.info("Mean Error (human): %s", me_human)
        logger.info("Median Absolute Error (human): %s", median_ae_human)

        print('MAE Per User:')
        field_lengths = [20, 10, 10, 10]
        headers = 'Username,MAE,Mean Error,Median AE'.split(',')
        data = tuple(str(v).ljust(field_lengths[i]) for i, v in enumerate(headers))
        print('%s\t%s\t%s\t%s' % data)
        for username in mae_per_human_true:
            _actual = mae_per_human_true[username]
            _pred = mae_per_human_pred[username]
            _mae = mean_absolute_error(_actual, _pred)
            _median_error = median_absolute_error(_actual, _pred)
            _mean_error = mean_error(_actual, _pred)
            data = (username, _mae, _mean_error, _median_error)
            data = tuple(str(v).ljust(field_lengths[i]) if isinstance(v, str) else str(round(v, 2)).ljust(field_lengths[i]) for i, v in enumerate(data))
            print('%s\t%s\t%s\t%s' % data)

        if human:
            return mae_human
        return mae_algo

    def apply(self, retrain=False, key=None, save=False):
        """
        Uses the last trained regressor and tags all untagged samples.

        retrain := If true, runs retrieve() and train() beforehand.
        key := specific ticket key to be tagged
        save := If true, updates the time estimate at the source
        """

        self.retrieve(training=False, key=key, has_label=False)

        df = self.get_untagged_dataframe()

        if retrain:
            self.retrieve()
            logger.info('Re-training regressor...')
            clf, count_vect, tfidf_transformer = self.train()
        else:
            clf = pickle.load(open(self.classifier_fn, 'rb'))
            count_vect = pickle.load(open(self.vectorizer_fn, 'rb'))
            tfidf_transformer = pickle.load(open(self.tfidf_fn, 'rb'))

        ret = []
        if df.empty:
            logger.info('No untagged records found.')
        else:
            for index, row in df.iterrows():
                # new_counts = count_vect.transform([row['text']])
                new_counts = count_vect.transform([row['title'] + ' ' + row['text']])
                X_new_tfidf = tfidf_transformer.transform(new_counts)
                prediction = clf.predict(X_new_tfidf)
                hours = prediction[0]
                if hours >= 1:
                    unit = 'h'
                    time_value = hours = int(round(hours, 0))
                elif hours <= 0:
                    # Assume a minimum of 15 minutes.
                    unit = 'm'
                    time_value = hours = 15
                else:
                    # Convert all hour fractions to minutes.
                    unit = 'm'
                    time_value = int(max(round(hours * 60, 0), self.minimum_estimate_minutes))

                logger.info('Key: %s %s', row['issue'], '%s%s' % (time_value, unit))
                ret.append((row['issue'], time_value, unit))
                if save:
                    if self.source == SOURCE_JIRA:
                        logger.info('Updating Jira ticket...')
                        issue = self.jira.issue(row['issue'])
                        prior_value = None
                        for field_name in self.hour_update_fields:
                            field_id = self.label_to_field[field_name]
                            prior_value = getattr(issue.fields, field_id)
                            if not prior_value:
                                issue.update(fields={field_id: hours})
                        if prior_value:
                            # If someone manually set an estimate in one of the optional auxillary fields, then use that estimate instead of our calculation.
                            time_value = prior_value
                            unit = 'h'
                            logger.info('Overriding estimate with existing entry of %s%s.', time_value, unit)
                        issue.update(fields={"timetracking": {"originalEstimate": "%s%s" % (time_value, unit)}})
                        logger.info('Jira ticket updated!')
                    else:
                        raise NotImplementedError('Unknown source: %s' % self.source)

        return ret

    def clean(self):
        logger.info('Cleaning.')
        os.system(f'rm -Rf {CACHE_DIR}/*')
        logger.info('Cleaned.')

    def test_login(self, ticket):
        """
        Does a readonly check of the login by attempting to retrieve info for a ticket.
        Note, the JIRA API doesn't have a direct authentication mechanism.
        If credentials are incorrect, the API will give a vague "record is missing or don't have permission" error.
        So we try querying details for a ticket we should definitely be able to access, and if we can't,
        then our login is bad.
        """
        logger.info('Testing login.')
        issue = self.jira.issue(ticket)
        logger.info('Project: %s', issue.fields.project.key)
        summary = issue.fields.summary
        logger.info('Summary: %s %s', ticket, summary)
        logger.info('Login tested.')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('config', default=None, help='Filename of the configuration file to use.')
    parser.add_argument('--verbose', default=False, action='store_true', help='Filename of the configuration file to use.')

    subparsers = parser.add_subparsers(
        dest="command",
    )

    retrieve_parser = subparsers.add_parser(ACTION_RETRIEVE, help='Retrieves data.')
    retrieve_parser.add_argument('--key', default='', help='Specific ticket key to tag.')

    train_parser = subparsers.add_parser(ACTION_TRAIN, help='Trains classifier on data.')

    test_parser = subparsers.add_parser(ACTION_TEST, help='Calculates the classifier\'s accuracy.')
    test_parser.add_argument('--retrain', default=False, action='store_true', help='If given, forces a retraining before tagging.')
    test_parser.add_argument('--human', default=False, action='store_true', help='If given, tests accuracy of human tagging.')
    test_parser.add_argument('--no-cache', default=False, action='store_true', help='If given, cached data will be ignored.')

    apply_parser = subparsers.add_parser(ACTION_APPLY, help='Uses the trained classifier to tag untagged data.')
    apply_parser.add_argument('--retrain', default=False, action='store_true', help='If given, forces a retraining before tagging.')
    apply_parser.add_argument('--key', default='', help='Specific ticket key to tag.')
    apply_parser.add_argument('--save', default=False, action='store_true', help='If given, saves the estimate in the source.')

    gc_parser = subparsers.add_parser(ACTION_GENERATE_COMBINATIONS, help='Generates base file of all classifier combinations to test.')

    tc_parser = subparsers.add_parser(ACTION_TEST_COMBINATIONS, help='Tests all classifier combinations.')

    lr_parser = subparsers.add_parser(ACTION_LIST_REGRESSORS, help='Lists all available regression algorithms.')

    clean_parser = subparsers.add_parser(ACTION_CLEAN, help='Clears all cached data.')

    tl_parser = subparsers.add_parser(ACTION_TEST_LOGIN, help='Check login.')
    tl_parser.add_argument('ticket', default='', help='Specific ticket key to test.')

    args = vars(parser.parse_args())
    # print('args:', args)

    config_fn = args.pop('config')
    verbose = args.pop('verbose')
    if not config_fn:
        raise Exception('No config specified.')
    estimator = Estimator.from_configuration_file(config_fn, verbose=verbose)

    cmd = args.pop('command')
    logger.info('Action: %s', cmd)
    if not cmd:
        raise Exception('No command specified.')
    if cmd not in ACTIONS:
        raise Exception('Invalid command: %s' % cmd)
    getattr(estimator, cmd.replace('-', '_'))(**args)


if __name__ == '__main__':
    main()
