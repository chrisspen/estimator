#!/bin/bash
set -e
VENV=.env
[ -d $VENV ] && rm -Rf $VENV
virtualenv -p python3.7 $VENV
. $VENV/bin/activate
pip install -U pip wheel setuptools
pip install -r requirements.txt requirements-test.txt
